This is a demonstration project to teach how to use git in a collaborative team environment.

The site is a simple "About Us" page. There is a main index.html page that uses jQuery to load 'cards' (which are basic html files) for each member of the team.

Individual 'cards' are stored in the 'team_profiles' directory, and named using the person's first and last name using snake_case followed by the .html suffix. For example, the card for Rick Smoke will be  'team_profiles/rick_smoke.html'.

Images for the team members are located in the 'images/team_members' directory and are named using the person's first and last name using snake_case followed by the .jpg suffix. There is a generic 'missing_image.jpg' file for images that do not exist or if a user chooses not to display their image.

The cards are based on a demo from http://www.creative-tim.com.


For this demo we are going to

1.) Do an initial set up of the project as a git repository
  git status
  git init
  git status
  git add .
  git status
  git commit -m "Initial Commit"
  git remote -v
  git remote add origin git@bitbucket.org:dchmura/mis_team.git
  git remote -v
  git status
  git push -u origin master

2.) Make an update to my card using a branch.
  git checkout -b update_daves_card
  git status
  -- edit card --
  git status
  git add team_profiles/dave_chmura.html
  -- alternatively cover git add -p --
  git commit -m "Update dave_chmura.html"
  git push origin update_daves_card

3.) Create a pull request on bitbucket.

4.) Merge the pull request

5.) Add collaborators

6.) Share with a collaborator so they can edit their card.


The bitbucket repository is located at.

git@bitbucket.org:lsamis/mis_team.git

